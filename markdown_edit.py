#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from markdown_editor import editor


def main():
    editor.main()

if __name__ == '__main__':
    main()
